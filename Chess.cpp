/* Chess.cpp
   David Glass, Greg Melick
   600.120
   5/3/17
   Final Project
   dglass7@jhu.edu, gmelick1@jhu.edu
*/

#undef FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"

int ChessGame::checkForCheck() {
    Position playerKing;
    Position opponentKing;
    //First we must find the location of the current player's king and the opponents king
    for (unsigned int y = 0; y < m_height; y++) {
        for (unsigned int x = 0; x < m_width; x++) {
            Position curr = Position(x, y);
            Piece * pie = this->getPiece(curr);
            // 5 is the id for king
            if (pie != nullptr && pie->id() == 5) {
                if(pie->owner() == this->playerTurn()) {
                    playerKing = curr;
                } else {
                    opponentKing = curr;
                }
            }
        }
    }
    bool playerInCheck = false;
    bool opponentInCheck = false;
    //Go through the board get every piece
    for (unsigned int y = 0; y < m_height; y++) {
        for (unsigned int x = 0; x < m_width; x++) {
            Position curr = Position(x, y);
            Piece * pie = this->getPiece(curr);
            //If the current piece can get the other teams king, change the corresponding boolean
            if (pie != nullptr && pie->owner() != this->playerTurn()) {
                int valid = pie->validMove(curr, playerKing, *this);
                if (valid >= 0) {
                    playerInCheck = true;
                }
            } else if (pie != nullptr) {
                int valid = pie->validMove(curr, opponentKing, *this);
                if (valid >= 0) {
                    opponentInCheck = true;
                }
            }
        }
    }
    //Because we made a move, the opponentInCheck is actually the current player being in check
    //Therefore opponentInCheck is more important than playerInCheck
    if (opponentInCheck) {
        return 1;
    } else if (playerInCheck) {
        //This only prints if the current player is not in check
        return -1;
    } else {
        //Neither player is in check
        return 0;
    }
}

int ChessGame::checkForCheckmate() {
    //If the current player cannot get out of their check, it is checkmate
    //First we must get every single piece the player has
    for (unsigned int i = 0; i < m_height; i++) {
        for (unsigned int j = 0; j < m_width; j++) {
            Position curr = Position(j, i);
            Piece * pie = this->getPiece(curr);
            if (pie != nullptr && pie->owner() == this->playerTurn()) {
                for (unsigned int y = 0; y < m_height; y++) {
                    for (unsigned int x = 0; x < m_width; x++) {
                        //For each piece, check every possible move it can make and see if any get the player out of check
                        Position newcurr = Position(x, y);
                        Piece * endPie = this->getPiece(newcurr);
                        if (pie->validMove(curr, newcurr, *this) >= 0) {
                            this->Board::makeMove(curr, newcurr);
                            //If checkForCheck is 1, that means the player is still in check
                            //Either way we must undo the move
                            if (this->checkForCheck() != 1) {
                                int startIndex = index(curr);
                                int endIndex = index(newcurr);
                                m_pieces[startIndex] = pie;
                                m_pieces[endIndex] = endPie;
                                m_turn--;
                                return 0;
                            } else {
                                int startIndex = index(curr);
                                int endIndex = index(newcurr);
                                m_pieces[startIndex] = pie;
                                m_pieces[endIndex] = endPie;
                                m_turn--;
                            }
                        }
                    }
                }
            }
        }
    }
    //Only reaches here if there is no way to escape the chess
    return 1;
}

int ChessGame::checkForStalemate() {
    //Goes through every piece owned by the current player, and sees if they have a move that does not put them in check
    for (unsigned int j = 0; j < m_height; j++) {
        for (unsigned int i = 0; i < m_width; i++) {
            Position curr = Position(i, j);
            Piece * pie = this->getPiece(curr);
            if (pie != nullptr && pie->owner() == this->playerTurn()) {
                //Go through every possible move the current piece can do
                for (unsigned int y = 0; y < m_height; y++) {
                    for (unsigned int x = 0; x < m_width; x++) {
                        Position newcurr = Position(x, y);
                        Piece * endPie = this->getPiece(newcurr);
                        if (pie->validMove(curr, newcurr, *this) >= 0) {
                            this->Board::makeMove(curr, newcurr);
                            //Either way you must undo the current move
                            if (this->checkForCheck() == 1) {
                                int startIndex = index(curr);
                                int endIndex = index(newcurr);
                                m_pieces[startIndex] = pie;
                                m_pieces[endIndex] = endPie;
                                m_turn--;
                            } else {
                                int startIndex = index(curr);
                                int endIndex = index(newcurr);
                                m_pieces[startIndex] = pie;
                                m_pieces[endIndex] = endPie;
                                m_turn--;
                                return 0;
                            }
                        }
                    }
                }
            }
        }
    }
    //Only reaches here if there is no valid move
    return 1;
}

int ChessGame::makeMove(Position start, Position end) {
    int check = this->checkForCheck();
    //This is if the current player starts their move in check
    if (check == -1) {
        Piece * startPie = this->getPiece(start);
        Piece * endPie = this->getPiece(end);
        int valid = this->Board::makeMove(start, end);
        //This case is if a player tries to move the other players piece
        if (valid > 0 && startPie != nullptr && startPie->owner() == this->playerTurn()) {
            valid = -12;
        }
        //If valid is >= 0 it is a valid move
        if (valid >= 0) {
            check = this->checkForCheck();
            //This means the the player did not handle their own check
            if (check == 1) {
                int endIndex = this->index(end);
                int startIndex = this->index(start);
                m_pieces[startIndex] = startPie;
                m_pieces[endIndex] = endPie;
                m_turn = m_turn - 1;
                return -3;
            } else if (check == -1) {
                //This means the player handled their check and put the other player in check
                //If valid = 7 pawn changes to a queen
                if (valid == 7) {
                    if (endPie != nullptr) {
                        valid = 3;
                    }
                    int endIndex = index(end);
                    Player owner = startPie->owner();
                    delete m_pieces[endIndex];
                    delete endPie;
                    m_pieces[endIndex] = nullptr;
                    //Create a pawn
                    this->Board::initPiece(4, owner, end);
                    return valid;
                }
                delete endPie;
                if (this->checkForCheckmate()) {
                    return 4;
                }
                return 2;
            } else {
                //Will enter this if the player handles their check but the other person is not in check
                if (valid == 7) {
                    if (endPie != nullptr) {
                        valid = 3;
                    }
                    int endIndex = index(end);
                    Player owner = startPie->owner();
                    delete m_pieces[endIndex];
                    delete endPie;
                    m_pieces[endIndex] = nullptr;
                    this->Board::initPiece(4, owner, end);
                    //We have to check if changing to a queen causes check or checkmate
                    if (this->checkForCheck() == -1) {
                        if (this->checkForCheckmate()) {
                            return 4;
                        }
                        return 2;
                    }
                    return valid;
                }
                delete endPie;
                if (this->checkForStalemate()) {
                    return 5;
                }
                return valid;
            }
        } else {
            //this is if the move is not valid
            if (valid == -12) {
                //player tried to move someone else's piece
                int endIndex = this->index(end);
                int startIndex = this->index(start);
                m_pieces[startIndex] = startPie;
                m_pieces[endIndex] = endPie;
                m_turn = m_turn - 1;
                return -6;
            }
            return valid;
        }
    } else if (check == 1) {
        //A player should never start their turn in check;
        std::cout << "You should not be here" << std::endl;
    } else {
        //This means no one was in check at the start of the turn
        Piece * startPie = this->getPiece(start);
        Piece * endPie = this->getPiece(end);
        int valid = this->Board::makeMove(start, end);
        if (valid > 0 && startPie != nullptr && startPie->owner() == this->playerTurn()) {
            valid = -12;
        }
        if (valid >= 0) {
            check = this->checkForCheck();
            //This means the player revealed themself to check
            if (check == 1) {
                int endIndex = this->index(end);
                int startIndex = this->index(start);
                m_pieces[startIndex] = startPie;
                m_pieces[endIndex] = endPie;
                m_turn = m_turn - 1;
                return -2;
            } else if (check == -1) {
                //This means the player got the other person in check
                if (valid == 7) {
                    if (endPie != nullptr) {
                        valid = 3;
                    }
                    int endIndex = index(end);
                    Player owner = startPie->owner();
                    delete m_pieces[endIndex];
                    delete endPie;
                    m_pieces[endIndex] = nullptr;
                    this->Board::initPiece(4, owner, end);
                    return valid;
                }
                delete endPie;
                if (this->checkForCheckmate()) {
                    return 4;
                }
                return 2;
            } else {
                //This means no one is in check
                if (valid == 7) {
                    if (endPie != nullptr) {
                        valid = 3;
                    }
                    int endIndex = index(end);
                    Player owner = startPie->owner();
                    delete m_pieces[endIndex];
                    delete endPie;
                    m_pieces[endIndex] = nullptr;
                    this->Board::initPiece(4, owner, end);
                    if (this->checkForCheck() == -1) {
                        if (this->checkForCheckmate()) {
                            return 4;
                        }
                        return 2;
                    }
                    return valid;
                }
                delete endPie;
                if (checkForStalemate()) {
                    return 5;
                }
                return valid;
            }
        } else {
            if (valid == -12) {
                int endIndex = this->index(end);
                int startIndex = this->index(start);
                m_pieces[startIndex] = startPie;
                m_pieces[endIndex] = endPie;
                m_turn = m_turn - 1;
                return -6;
            }
            return valid;
        }
    }
    return 0;
}

// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, WHITE, Position(i, 1));
        initPiece(pieces[i], WHITE, Position(i, 0));
        initPiece(pieces[i], BLACK, Position(i, 7));
        initPiece(PAWN_ENUM, BLACK, Position(i, 6));
    }
}

void ChessGame::printBoard() {
    std::cout << " ";
    //Prints out the letters that represent the columns
    for (unsigned int k = 0; k < m_width; k++) {
        char c = 'a' + k;
        std::cout << " " << c << " ";
    }
    std::cout << std::endl;
    //Go through and print out the board, with red and blue pieces
    for (int y = (m_height - 1); y >= 0; y--) {
        std::cout << y + 1;
        for (unsigned int x = 0; x < m_width; x++) {
            if ((x + y) % 2 == 0) {
                Terminal::colorBg(Terminal::Color::RED);
            } else {
                Terminal::colorBg(Terminal::Color::CYAN);
            }
            Position p = Position(x, y);
            Piece* pie = this->getPiece(p);
            this->printPieceID(pie);
        }
        //Reset to default color
        Terminal::set_default();
        std::cout << y + 1 << std::endl;
    }
    std::cout << " ";
    for (unsigned int k = 0; k < m_width; k++) {
        char c = 'a' + k;
        std::cout << " " << c << " ";
    }
    std::cout << std::endl;
}

void ChessGame::printPieceID(Piece* pie) {
    if (pie == nullptr) {
        std::cout << "   ";
        return;
    }
    //Prints white pieces white and black pieces black
    if (pie->owner() == WHITE) {
        Terminal::colorFg(1, Terminal::Color::WHITE);
    } else {
        Terminal::colorFg(0, Terminal::Color::BLACK);
    }
    int id = pie->id();
    switch(id) {
    case 0:
        std::cout << " ♙ ";
        break;
    case 1:
        std::cout << " ♖ ";
        break;
    case 2:
        std::cout << " ♘ ";
        break;
    case 3:
        std::cout << " ♗ ";
        break;
    case 4:
        std::cout << " ♕ ";
        break;
    case 5:
        std::cout << " ♔ ";
        break;
    default:
        std::cout << "   ";
    }
}

void ChessGame::save(std::string fileName) {

    std::ofstream saveFile;
    saveFile.open(fileName);
    saveFile << "chess\n";
    saveFile << this->m_turn - 1 << "\n";
    for (int y = (m_height - 1); y >= 0; y--) {
        for (unsigned int x = 0; x < m_width; x++) {
            Position p = Position(x, y);
            Piece* pie = this->getPiece(p);
            if (pie != nullptr) {
                saveFile << pie->owner() << " ";
                char xPos = 'a' + x;
                int yPos = y + 1;
                saveFile << xPos << yPos << " ";
                saveFile << pie->id() << std::endl;
            }
        }
    }
    saveFile.close();
}

int ChessGame::loadGameValid(std::string fileName) {
    std::ifstream loadFile;
    loadFile.open(fileName);
    std::string typeOfGame;
    std::getline(loadFile, typeOfGame);
    if (typeOfGame.compare("chess")) {
        return 0;
    }
    return 1;
}

int ChessGame::loadTurnValid(std::string fileName) {
    std::ifstream loadFile;
    loadFile.open(fileName);
    std::string typeOfGame;
    std::getline(loadFile, typeOfGame);
    int turn = -1;
    std::string turnStr;
    std::getline(loadFile, turnStr);
    turn = atoi(turnStr.c_str());
    if (turn < 0) {
        return 0;
    }
    return turn + 1;
}

void ChessGame::load(std::string fileName) {

    std::ifstream loadFile;
    loadFile.open(fileName);
    std::string typeOfGame;
    std::getline(loadFile, typeOfGame);
    std::string turnStr;
    std::getline(loadFile, turnStr);
    int turn = 0;
    turn = atoi(turnStr.c_str());
    this->m_turn = turn + 1;

    std::string line;
    int owner;
    Player p;
    int xPos;
    int yPos;
    int id;

    while (std::getline(loadFile, line)) {
        owner = line[0] - '0';
        if (owner == 0) {
            p = WHITE;
        } else {
            p = BLACK;
        }
        xPos = line[2] - 'a';
        yPos = line[3] - '1';
        id = line[5] - '0';
        initPiece(id, p, Position(xPos, yPos));
    }
}


void ChessGame::callPrompts(int i) {

    switch (i) {
    case -7:
        Prompts::outOfBounds();
        break;
    case -6:
        Prompts::noPiece();
        break;
    case -5:
        Prompts::blocked();
        break;
    case -4:
        Prompts::cantCastle();
        break;
    case -3:
        Prompts::mustHandleCheck();
        break;
    case -2:
        Prompts::cantExposeCheck();
        break;
    case -1:
        Prompts::illegalMove();
        break;
    case 2:
        m_turn--;
        Prompts::check(this->playerTurn());
        m_turn++;
        break;
    case 3:
        m_turn--;
        Prompts::capture(this->playerTurn());
        m_turn++;
        break;
    case 4:
        m_turn--;
        Prompts::checkMate(this->playerTurn());
        Prompts::win(this->playerTurn(), m_turn);
        break;
    case 5:
        Prompts::staleMate();
        break;
    }
}
