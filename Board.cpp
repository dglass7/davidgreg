/* Board.cpp
   David Glass, Greg Melick
   600.120
   5/3/17
   Final Project
   dglass7@jhu.edu, gmelick1@jhu.edu
*/

#include <assert.h>
#include <cstdio>
#include <cctype>
#include <iostream>
#include <string>
#include "Game.h"
#include "Prompts.h"
#include "Terminal.h"

///////////////
// Board //
///////////////

Board::~Board() {
    // Delete all pointer-based resources
    for (unsigned int i=0; i < m_width * m_height; i++)
        delete m_pieces[i];
    for (size_t i=0; i < m_registeredFactories.size(); i++)
        delete m_registeredFactories[i];
}

// Get the Piece at a specific Position, or nullptr if there is no
// Piece there or if out of bounds.
Piece* Board::getPiece(Position position) const {
    if (validPosition(position))
        return m_pieces[index(position)];
    else {
        return nullptr;
    }
}

// Create a piece on the board using the factory.
// Returns true if the piece was successfully placed on the board
bool Board::initPiece(int id, Player owner, Position position) {
    Piece* piece = newPiece(id, owner);
    if (!piece) return false;

    // Fail if the position is out of bounds
    if (!validPosition(position)) {
        Prompts::outOfBounds();
        return false;
    }
    // Fail if the position is occupied
    if (getPiece(position)) {
        Prompts::blocked();
        return false;
    }
    m_pieces[index(position)] = piece;
    return true;
}

// Add a factory to the Board to enable producing
// a certain type of piece
bool Board::addFactory(AbstractPieceFactory* pGen) {
    // Temporary piece to get the ID
    Piece* p = pGen->newPiece(WHITE);
    int id = p->id();
    delete p;

    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        m_registeredFactories[id] = pGen;
        return true;
    } else {
        std::cout << "Id " << id << " already has a generator\n";
        return false;
    }
}

// Search the factories to find a factory that can translate `id' to
// a Piece, and use it to create the Piece. Returns nullptr if not found.
Piece* Board::newPiece(int id, Player owner) {
    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        std::cout << "Id " << id << " has no generator\n";
        return nullptr;
    } else {
        return it->second->newPiece(owner);
    }
}

int Board::makeMove(Position start, Position end) {
    Piece * startPie = getPiece(start);
    //Moves a piece from start to end if the move is valid
    if (startPie != nullptr) {
        int valid = startPie->validMove(start, end, *this);
        if (valid >= 0) {
            int startIndex(this->index(start));
            int endIndex(this->index(end));
            m_pieces[endIndex] = m_pieces[startIndex];
            m_pieces[startIndex] = nullptr;
            m_turn++;
        }
        return valid;
    } else {
        if (validPosition(start)) {
            return -6;
        } else {
            return -7;
        }
    }
}

int Board::run() {
    //Gives the initial prompt and collects 1 or 2 as valid input
    int startGame = 0;
    char input[128];
    Prompts::menu();
    while (!startGame) {
        fgets(input, 128, stdin);
        if (input[1] == '\n') {
            if (input[0] == '1') {
                return 1;
            } else if (input[0] == '2') {
                return 2;
            }
        }
        Prompts::menu();
    }
    //Should never hit this
    return 0;
}
