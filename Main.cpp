/* Main.cpp
   David Glass, Greg Melick
   600.120
   5/3/17
   Final Project
   dglass7@jhu.edu, gmelick1@jhu.edu
*/

#undef FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cctype>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"

int main() {
    ChessGame chess;
    int newOrLoadedGame = chess.run();

    if (newOrLoadedGame == 1) {
        chess.setupBoard();
    } else {
        Prompts::loadGame();
        std::string loadFile;
        std::getline(std::cin, loadFile);
        int isLoadGameValid = chess.loadGameValid(loadFile);
        int isLoadTurnValid = chess.loadTurnValid(loadFile);
        if (!isLoadGameValid || !isLoadTurnValid) {
            Prompts::loadFailure();
            return 1;
        }
        chess.load(loadFile);
    }

    int exit = 0;
    int showBoard = 0;
    int inputIsValid = 1;
    std::string input;
    std::string command;
    while (!exit) {
        Prompts::playerPrompt(chess.playerTurn(), chess.m_turn);
        std::getline(std::cin, input);
        //Make all user input lowercase to all for case-insensitivity
        for (unsigned int i = 0; i < input.length(); i++) {
            input[i] = tolower(input[i]);
        }
        //Input is invalid until proven valid
        inputIsValid = 0;
        //Check if player wants to quit
        command = input.substr(0,1);
        if (!command.compare("q")) {
            inputIsValid = 1;
            //Do not really need this next line
            exit = 1;
            break;
        }
        //Check if player wants to toggle board
        command = input.substr(0,5);
        if (!command.compare("board")) {
            inputIsValid = 1;
            showBoard = !showBoard;
        }
        //Check if player wants to save current game
        command = input.substr(0,4);
        if (!command.compare("save")) {
            Prompts::saveGame();
            std::string saveFileName;
            std::getline(std::cin, saveFileName);
            chess.save(saveFileName);
            continue;
        }
        //Check if player wants to forfeit
        command = input.substr(0,7);
        if (!command.compare("forfeit")) {
            if (chess.m_turn % 2 == 0) {
                Prompts::win(WHITE, chess.m_turn);
            } else {
                Prompts::win(BLACK, chess.m_turn);
            }
            Prompts::gameOver();
            break;
        }
        //Check if player wants to make valid move
        if (isalpha(input[0]) && isdigit(input[1]) && isspace(input[2])
                && isalpha(input[3]) && isdigit(input[4])) {
            inputIsValid = 1;
            int startColumn(input[0] - 'a');
            int startRow(input[1] - '1');
            int endColumn(input[3] - 'a');
            int endRow(input[4] - '1');
            Position startP = Position(startColumn, startRow);
            Position endP = Position(endColumn, endRow);
            int valid = chess.makeMove(startP, endP);
            chess.callPrompts(valid);
            if (valid == 4 || valid == 5) {
                Prompts::gameOver();
                break;
            }
        }
        //If player enters invalid input
        if (!inputIsValid) {
            Prompts::parseError();
            continue;
        }
        //Check if board is toggled on
        if (showBoard) {
            chess.printBoard();
        }
    }
    return 0;
}
