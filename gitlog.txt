commit 67513ad074f613c5ca0aae3a129227df5524b601
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu May 4 11:20:22 2017 -0400

    Fixed save/load issue

commit def7b87a6b5ef84b3220f3cddc0b723d59b810d2
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu May 4 10:16:34 2017 -0400

    Fixed pawns

commit 6e45761927f538988fd7a125bfe171d195b96fb8
Author: David Glass <david.michael.glass@gmail.com>
Date:   Thu May 4 10:11:18 2017 -0400

    First test done

commit 3d2e7ecebb3dacfde109e407629bea2ffaf83121
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu May 4 09:51:57 2017 -0400

    Fixed Poor little pawn that wants to be a queen

commit 3fe6cd2839bec9cb6724030984dd7db36dbf1279
Author: David Glass <david.michael.glass@gmail.com>
Date:   Thu May 4 09:28:26 2017 -0400

    Updated to do list

commit d6e315a43ee25399ea626c106089dfe6e7a09f17
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu May 4 09:26:30 2017 -0400

    No Leakage!

commit 0217c162d8ba6bf49c9a1949d9b5125324a4a5b0
Author: David Glass <david.michael.glass@gmail.com>
Date:   Thu May 4 02:07:10 2017 -0400

    Updated to do list

commit e3b33a1893f1cb52cfc4a9904785977c84cc396a
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu May 4 01:39:43 2017 -0400

    added example commands file

commit 46dc88a48d6e1ca0f6fce5033c51ab4d273531df
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu May 4 01:34:36 2017 -0400

    FULL FUNCTIONALITy

commit 0a94c57a603ac739f5257fa47cd92d2748b42c8b
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu May 4 01:09:48 2017 -0400

    astyled

commit 970c10c7322dfeea568380464dc6f31f5e661d34
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu May 4 01:09:22 2017 -0400

    astyled

commit dab543b9bb3f2beb37ffb451be9e4b82e5716156
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu May 4 01:05:58 2017 -0400

    Checkmate Works

commit fbe63dda8164ecd18cbb88fecba78d93ccbd205b
Author: David Glass <david.michael.glass@gmail.com>
Date:   Wed May 3 19:41:56 2017 -0400

    Working on test shell

commit a39253b83e37db6c7db006ea4aff00fb3694338d
Author: David Glass <david.michael.glass@gmail.com>
Date:   Wed May 3 14:44:40 2017 -0400

    ID info put in all our files

commit ca62f5e099233c3706e26a73a264c238bce88640
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Wed May 3 12:59:49 2017 -0400

    Pawns turn into queens

commit 5fa265663421ad0f4ceb7d44d421e7a4d78e7c47
Author: David Glass <david.michael.glass@gmail.com>
Date:   Wed May 3 00:46:07 2017 -0400

    goodnight

commit 032c3716427e6b5bfb856287482c0981cff31902
Author: David Glass <david.michael.glass@gmail.com>
Date:   Wed May 3 00:28:44 2017 -0400

    Load pretty much done

commit 87822123dfd52a051f1272c0672956c3e59fec76
Author: David Glass <david.michael.glass@gmail.com>
Date:   Tue May 2 23:15:06 2017 -0400

    More progress to load

commit bafddc279fdb04946d7bdf0315c0f32d0b61a179
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Tue May 2 20:16:54 2017 -0400

    Check Works

commit b4821344642c79b0e89edf7d410fdde4a187c654
Author: David Glass <david.michael.glass@gmail.com>
Date:   Tue May 2 13:23:28 2017 -0400

    More progress to load method

commit b40ac3e7cada21fa5fb4f5bd87bd39ce07afd994
Author: David Glass <david.michael.glass@gmail.com>
Date:   Tue May 2 11:38:00 2017 -0400

    progress made to load

commit df98167b65f042cd85d036adbe086eeba6c31b43
Author: David Glass <david.michael.glass@gmail.com>
Date:   Tue May 2 09:32:38 2017 -0400

    Save method worksgit add .

commit 2b3674fe22b4f5f004bd52b93bb004300d59eb5b
Author: David Glass <david.michael.glass@gmail.com>
Date:   Tue May 2 02:25:24 2017 -0400

    Beginnings of save function

commit 629657bd7aa3f60eba10daadd9872607e3bfd611
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Tue May 2 01:52:17 2017 -0400

    Fixed

commit d215cc1120f913d1f8c34b4879897a871251c338
Author: David Glass <david.michael.glass@gmail.com>
Date:   Tue May 2 01:45:13 2017 -0400

    Updated Main again

commit 2659b5005b8272358375007444444463ce5c9357
Merge: 18dbef5 af457ce
Author: David Glass <david.michael.glass@gmail.com>
Date:   Tue May 2 01:42:51 2017 -0400

    Updated Main

commit 18dbef51dd05635072eaf9917649656ea400cef1
Author: David Glass <david.michael.glass@gmail.com>
Date:   Tue May 2 01:41:09 2017 -0400

    Board now defaults to off toggle

commit af457ce705db73a203ee482dfb060e6123ee4208
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Tue May 2 01:38:57 2017 -0400

    Removed Seg Fault

commit f48781e0a907891f8a041e9b7046c5b9d1c5e848
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Tue May 2 01:38:29 2017 -0400

    Fixed Seg Fault

commit cb743104cbbd2d1079c03777fdbbf69284c373c4
Author: David Glass <david.michael.glass@gmail.com>
Date:   Mon May 1 23:00:03 2017 -0400

    Just in case

commit 6e85319e1be7572ce241b39c7b19d789f0c6693f
Author: David Glass <david.michael.glass@gmail.com>
Date:   Mon May 1 21:48:25 2017 -0400

    Forfeit command and invalid input are handled properly

commit 482b0cc359c3a6a2c96a01271d2902de85457a65
Author: David Glass <david.michael.glass@gmail.com>
Date:   Mon May 1 15:20:28 2017 -0400

    Board Toggle and Quit commands work properly

commit 3f25544db291036e89d81c8f5ca878e398d1a687
Author: David Glass <david.michael.glass@gmail.com>
Date:   Mon May 1 14:44:47 2017 -0400

    Board::run works properly

commit 2a8ce53e12560d1780b8f1f7f9fdbaba868f6e35
Author: David Glass <david.michael.glass@gmail.com>
Date:   Sun Apr 30 23:43:18 2017 -0400

    Changed to-do list

commit 49b768c386ce11bf754e8d5bf6d92253ba2017ab
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Sun Apr 30 23:31:58 2017 -0400

    Defaulted

commit 17736b4725957af5b24c78401a396b8e474a1e91
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Sun Apr 30 23:25:14 2017 -0400

    Black text everywhere

commit f8e71e10f12840a7d5ed961265db32c80d30ce10
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Sun Apr 30 23:19:06 2017 -0400

    Default now goes to white

commit aa75f13888fd54e48f62fcdcf2d559bfc5fbb284
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Sun Apr 30 23:16:08 2017 -0400

    Better letters

commit 75e90874d0e14c75d9bb9db58f39d7baacef0e3a
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Sun Apr 30 23:13:47 2017 -0400

    Better Board

commit 144b5adb7b35964b880fb71c8e8d96bac66d06ce
Author: David Glass <david.michael.glass@gmail.com>
Date:   Sun Apr 30 23:07:57 2017 -0400

    Added Main.cpp and rearranged Board and Chess

commit 107b4fbf35d0a047f277bc000873996985911099
Author: David Glass <david.michael.glass@gmail.com>
Date:   Sun Apr 30 19:53:40 2017 -0400

    final updates to printBoard

commit 963ef10770c8c505a9598f3e2d07a7acf23557fc
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Sun Apr 30 17:22:07 2017 -0400

    All Piece Functionality Finished

commit 6e21b05fdcbff2446342be08cd3d4b133a57b780
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Sun Apr 30 16:06:10 2017 -0400

    Only King and Queen Functionality left

commit 6e0d4e70d7ed0fb3ac0522648e200ff1a8fa88c9
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Sat Apr 29 10:55:48 2017 -0400

    Added Rook Functionality

commit a766a955bd608ecef8d2dcae048157da8001761e
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Sat Apr 29 10:55:38 2017 -0400

    Added Rook Functionality

commit 1632cde36d20ac3abe5ece52b51789f806d1b71e
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Sat Apr 29 01:55:18 2017 -0400

    Added a to-do list

commit 9544d5d99db50408da20017071d7d8b9ce839629
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Sat Apr 29 01:41:21 2017 -0400

    Pawn movement dictated

commit fab7b030467ad504d6d21e838e689acf9b356c52
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Fri Apr 28 14:17:33 2017 -0400

    Printing board in color works

commit 8697ac247f34a0701195209c24343490cdd50f8c
Author: David Glass <david.michael.glass@gmail.com>
Date:   Fri Apr 28 13:17:17 2017 -0400

    trying to fix printBoard method

commit 2b5f8e312e3f296b882640afb85ccec25bf1abcc
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu Apr 27 13:53:01 2017 -0400

    Starts checking move validity

commit 89b682abc1cfc4b264f95ec7632daea224a3bac6
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu Apr 27 11:39:24 2017 -0400

    Correct Prompt for move

commit 437c7a883b7f6ebc6a9283f1a6c4a335867eddf9
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu Apr 27 11:39:07 2017 -0400

    Correct Prompt for move

commit f575ad9cd14f9c611d24555d5a2f80f721ba8ec6
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu Apr 27 11:26:05 2017 -0400

    WE CAN MOVE PIECES

commit 2a362316fafd8312faad8fad18d255e6e1687803
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu Apr 27 11:25:05 2017 -0400

    WE CAN MOVE PIECES

commit eda9921fde7bd2befb16cd38c10c36ac22512e3f
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu Apr 27 10:17:22 2017 -0400

    Correctly Prints Board

commit cc34d1b4c74b68eb2497bc04993ec02056754a45
Author: Greg Melick <gregorymelick@gmail.com>
Date:   Thu Apr 27 09:33:50 2017 -0400

    Updated Files

commit 5449eb923bc154608e5d914b24621a0d14a99a15
Author: David Glass <david.michael.glass@gmail.com>
Date:   Sat Apr 22 15:56:09 2017 -0400

    downloaded given files

commit 6ca86edd4315f601e6341e627b2b6a04e1ee973c
Author: David Glass <david.michael.glass@gmail.com>
Date:   Sat Apr 22 15:41:11 2017 -0400

    start of final gitlog
