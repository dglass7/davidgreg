#Makefile
#David Glass, Greg Melick
#600.120
#5/3/17
#Final Project
#dglass7@jhu.edu, gmelick1@jhu.edu

all: chess unittest

CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11 -g

chess: Chess.o Board.o Pieces.o Main.o
	$(CXX) $(CXXFLAGS) Chess.o Board.o Pieces.o Main.o -o chess

unittest: Chess.o Board.o Pieces.o Unittest.o
	$(CXX) $(CXXFLAGS) Chess.o Board.o Pieces.o Unittest.o -o unittest

Pieces.o: Game.h Chess.h
Board.o: Game.h
Chess.o: Game.h Chess.h Prompts.h Terminal.h
Main.o: Game.h Chess.h Prompts.h Terminal.h
Unittest.o: Game.h Chess.h Prompts.h

clean:
	rm *.o *.orig chess unittest

