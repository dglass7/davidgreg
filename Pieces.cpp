/* Pieces.cpp
   David Glass, Greg Melick
   600.120
   5/3/17
   Final Project
   dglass7@jhu.edu, gmelick1@jhu.edu
*/

#include <iostream>

#include "Game.h"
#include "Chess.h"
#include "Prompts.h"

int Piece::validMove(Position start, Position end, const Board& Board) const {
    //This will be an out of bounds error
    if (!Board.validPosition(start) || !Board.validPosition(end)) {
        return -7;
    }
    //This is if a player tries to move a space with no piece
    if (Board.getPiece(start) == nullptr) {
        return -6;
    }
    //This makes sure a player does not try to move a piece to their same spot
    if (Board.getPiece(start) == Board.getPiece(end)) {
        return -1;
    }
    return 1;
}

int Pawn::validMove(Position start, Position end, const Board& Board) const {
    int generalValidity = this->Piece::validMove(start, end, Board);
    if (generalValidity < 0) {
        return generalValidity;
    }
    Piece * pie = Board.getPiece(end);
    //The pawn is not capturing a piece
    if (pie == nullptr) {
        //Make sure it is not moving sideways
        if (end.x - start.x == 0) {
            //Make sure it only moves one up or down depending on the owner
            if ((m_owner == WHITE && end.y - start.y == 1) || (m_owner == BLACK && (int)end.y - (int)start.y == -1)) {
                //This only happens if the pawn needs to turn into a queen
                if ((m_owner == WHITE && end.y == 7) || (m_owner == BLACK && end.y == 0)) {
                    return 7;
                }
                return 1;
                //This is only valid if it is the pawns first move
            } else if ((m_owner == WHITE && end.y - start.y == 2 && start.y == 1) || (m_owner == BLACK && (int)end.y - (int)start.y == -2 && start.y == 6)) {
                if (Board.getPiece(Position(start.x, (end.y + start.y) / 2)) != nullptr) {
                    return -5;
                }
                return 1;
            }
        }
        //This is for when the pawn is capturing a piece
    } else {
        //It must move one up and one over and it cannot catch one of its own pieces
        if (end.x - start.x == 1 || (int)end.x - (int)start.x == -1) {
            if ((m_owner == WHITE && end.y - start.y == 1) || (m_owner == BLACK && (int)end.y - (int)start.y == -1)) {
                if (pie->owner() != m_owner) {
                    //Pawn can still turn into a queen
                    if ((m_owner == WHITE && end.y == 7) || (m_owner == BLACK && end.y == 0)) {
                        return 7;
                    }
                    return 3;
                } else {
                    return -5;
                }
            }
        }
    }
    return -1;
}

int Rook::validMove(Position start, Position end, const Board& Board) const {
    int generalValidity = this->Piece::validMove(start, end, Board);
    if (generalValidity < 0) {
        return generalValidity;
    }
    Piece * pie = Board.getPiece(end);
    //Either the x values stay the same or the y values stay the same
    if (start.x == end.x) {
        //Checks if the move is blocked for both moving up and down
        if ((int)end.y - (int)start.y > 0) {
            for (unsigned int i = start.y + 1; i < end.y; i++) {
                Piece * pie2 = Board.getPiece(Position(start.x, i));
                if (pie2 != nullptr) {
                    return -5;
                }
            }
        } else {
            for (unsigned int i = start.y - 1; i > end.y; i--) {
                Piece * pie2 = Board.getPiece(Position(start.x, i));
                if (pie2 != nullptr) {
                    return -5;
                }
            }
        }
    } else if (start.y == end.y) {
        //Checks if the move is blocked for both moving left and right
        if ((int)end.x - (int)start.x > 0) {
            for (unsigned int j = start.x + 1; j < end.x; j++) {
                Piece * pie2 = Board.getPiece(Position(j, start.y));
                if (pie2 != nullptr) {
                    return -5;
                }
            }
        } else {
            for (unsigned int j = start.x - 1; j > end.x; j--) {
                Piece * pie2 = Board.getPiece(Position(j, start.y));
                if (pie2 != nullptr) {
                    return -5;
                }
            }
        }
    } else {
        //This means both x and y are changing
        return -1;
    }
    //This determines if someone is grabbing their own piece or if they're capturing a piece
    if (pie != nullptr) {
        if (pie->owner() == m_owner) {
            return -5;
        } else {
            return 3;
        }
    }
    return 1;
}

int Knight::validMove(Position start, Position end, const Board& Board) const {
    int generalValidity = this->Piece::validMove(start, end, Board);
    if (generalValidity < 0) {
        return generalValidity;
    }
    //Knight can move any combination of one up or down and two over, or two up or down and one over
    if (end.x - start.x == 1 || (int)end.x - (int)start.x == -1) {
        if (end.y - start.y != 2 && (int)end.y - (int)start.y != -2) {
            return -1;
        }
    } else if (end.x - start.x == 2 || (int)end.x - (int)start.x == -2) {
        if (end.y - start.y != 1 && (int)end.y - (int)start.y != -1) {
            return -1;
        }
    } else {
        return -1;
    }
    Piece * endPie = Board.getPiece(end);
    if (endPie != nullptr) {
        if (endPie->owner() == m_owner) {
            return -5;
        } else {
            return 3;
        }
    }
    return 1;
}

int Bishop::validMove(Position start, Position end, const Board& Board) const {
    int generalValidity = this->Piece::validMove(start, end, Board);
    if (generalValidity < 0) {
        return generalValidity;
    }
    //Change in x must equal change in y
    if ((int)end.x - (int)start.x != (int)end.y - (int)start.y && (int)end.x - (int)start.x != -1 * ((int)end.y - (int) start.y)) {
        return -2;
    }
    //All the cases of movement to check if the movement is blocked
    if ((int)end.x - (int)start.x > 0) {
        if ((int)end.y - (int)start.y > 0) {
            unsigned int i = start.x + 1;
            for (unsigned int j = start.y + 1; j < end.y; j++) {
                if (Board.getPiece(Position(i, j)) != nullptr) {
                    return -5;
                }
                i++;
            }
        } else {
            unsigned int i = start.x + 1;
            for (unsigned int j = start.y - 1; j > end.y; j--) {
                if (Board.getPiece(Position(i, j)) != nullptr) {
                    return -5;
                }
                i++;
            }
        }
    } else {
        if ((int)end.y - (int)start.y > 0) {
            unsigned int i = start.x - 1;
            for (unsigned int j = start.y + 1; j < end.y; j++) {
                if (Board.getPiece(Position(i, j)) != nullptr) {
                    return -3;
                }
                i--;
            }
        } else {
            unsigned int i = start.x - 1;
            for (unsigned int j = start.y - 1; j > end.y; j--) {
                if (Board.getPiece(Position(i, j)) != nullptr) {
                    return -3;
                }
                i--;
            }
        }
    }
    Piece * endPie = Board.getPiece(end);
    if (endPie != nullptr) {
        if (endPie->owner() == m_owner) {
            return -5;
        } else {
            return 3;
        }
    }
    return 1;
}

int Queen::validMove(Position start, Position end, const Board& Board) const {
    int generalValidity = this->Piece::validMove(start, end, Board);
    if (generalValidity < 0) {
        return generalValidity;
    }
    //Queen is basically a combination of rook and bishop
    Piece * pie = Board.getPiece(end);
    if (start.x == end.x) {
        if ((int)end.y - (int)start.y > 0) {
            for (unsigned int i = start.y + 1; i < end.y; i++) {
                Piece * pie2 = Board.getPiece(Position(start.x, i));
                if (pie2 != nullptr) {
                    return -5;
                }
            }
        } else {
            for (unsigned int i = start.y - 1; i > end.y; i--) {
                Piece * pie2 = Board.getPiece(Position(start.x, i));
                if (pie2 != nullptr) {
                    return -5;
                }
            }
        }
    } else if (start.y == end.y) {
        if ((int)end.x - (int)start.x > 0) {
            for (unsigned int j = start.x + 1; j < end.x; j++) {
                Piece * pie2 = Board.getPiece(Position(j, start.y));
                if (pie2 != nullptr) {
                    return -5;
                }
            }
        } else {
            for (unsigned int j = start.x - 1; j > end.x; j--) {
                Piece * pie2 = Board.getPiece(Position(j, start.y));
                if (pie2 != nullptr) {
                    return -5;
                }
            }
        }
    } else if ((int)end.x - (int)start.x != (int)end.y - (int)start.y && (int)end.x - (int)start.x != -1 * ((int)end.y - (int) start.y)) {
        return -1;
    } else {
        if ((int)end.x - (int)start.x > 0) {
            if ((int)end.y - (int)start.y > 0) {
                unsigned int i = start.x + 1;
                for (unsigned int j = start.y + 1; j < end.y; j++) {
                    if (Board.getPiece(Position(i, j)) != nullptr) {
                        return -5;
                    }
                    i++;
                }
            } else {
                unsigned int i = start.x + 1;
                for (unsigned int j = start.y - 1; j > end.y; j--) {
                    if (Board.getPiece(Position(i, j)) != nullptr) {
                        return -5;
                    }
                    i++;
                }
            }
        } else {
            if ((int)end.y - (int)start.y > 0) {
                unsigned int i = start.x - 1;
                for (unsigned int j = start.y + 1; j < end.y; j++) {
                    if (Board.getPiece(Position(i, j)) != nullptr) {
                        return -5;
                    }
                    i--;
                }
            } else {
                unsigned int i = start.x - 1;
                for (unsigned int j = start.y - 1; j > end.y; j--) {
                    if (Board.getPiece(Position(i, j)) != nullptr) {
                        return -5;
                    }
                    i--;
                }
            }
        }
    }
    if (pie != nullptr) {
        if (pie->owner() == m_owner) {
            return -5;
        } else {
            return 3;
        }
    }
    return 1;
}

int King::validMove(Position start, Position end, const Board& Board) const {
    int generalValidity = this->Piece::validMove(start, end, Board);
    if (generalValidity < 0) {
        return generalValidity;
    }
    Piece * endPiece = Board.getPiece(end);
    //King can only move one spot at a time, so the change in x cannot be greater than 1 and change in y cannot be greater than 1
    if ((int)end.x - (int)start.x > 1 || (int)end.x - (int)start.x < -1) {
        return -1;
    } else if ((int)end.y - (int)start.y > 1 || (int)end.y - (int)start.y < -1) {
        return -1;
    }
    if (endPiece != nullptr) {
        if (endPiece->owner() == m_owner) {
            return -5;
        } else {
            return 3;
        }
    }
    return 1;
}
