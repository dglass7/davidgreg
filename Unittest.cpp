/* Unittest.cpp
   David Glass, Greg Melick
   600.120
   5/5/17
   Final Project
   dglass7@jhu.edu, gmelick1@jhu.edu
*/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cctype>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include <cassert>

void testLoadGameFails();
void testLoadGameWorks();
void testLoadTurnFails();
void testLoadTurnWorks();
void testCheckForCheckFails();
void testCheckForCheckWorks();
void testCheckForCheckmateFails();
void testCheckForCheckmateWorks();
void testCheckForStalemateFails();
void testCheckForStalemateWorks();
void testValidMoveFails();
void testValidMoveWorks();

int main() {
    testLoadGameFails();
    testLoadGameWorks();
    testLoadTurnFails();
    testLoadTurnWorks();
    testCheckForCheckFails();
    testCheckForCheckWorks();
    testCheckForCheckmateFails();
    testCheckForCheckmateWorks();
    testCheckForStalemateFails();
    testCheckForStalemateWorks();
    testValidMoveFails();
    testValidMoveWorks();
}

void testLoadGameFails() {
    ChessGame chess = ChessGame();
    int test = chess.loadGameValid("loadGameFails.txt");
    assert(test == 0);
}

void testLoadGameWorks() {
    ChessGame chess = ChessGame();
    int test = chess.loadGameValid("loadGameWorks.txt");
    assert(test == 1);
}

void testLoadTurnFails() {
    ChessGame chess = ChessGame();
    int test = chess.loadTurnValid("loadTurnFails.txt");
    assert(test == 0);
}

void testLoadTurnWorks() {
    ChessGame chess = ChessGame();
    int test = chess.loadTurnValid("loadTurnWorks.txt");
    assert(test == 17);
}

void testCheckForCheckFails() {
    ChessGame chess = ChessGame();
    chess.load("newgame.txt");
    int test = chess.checkForCheck();
    assert(test == 0);
}

void testCheckForCheckWorks() {
    ChessGame chess = ChessGame();
    chess.load("blackInCheck.txt");
    int test = chess.checkForCheck();
    assert(test == -1);
}

void testCheckForCheckmateFails() {
    ChessGame chess = ChessGame();
    chess.load("newgame.txt");
    int test = chess.checkForCheckmate();
    assert(test == 0);
}

void testCheckForCheckmateWorks() {
    ChessGame chess = ChessGame();
    chess.load("currentCheckmate");
    int test = chess.checkForCheckmate();
    assert(test == 1);
}

void testCheckForStalemateFails() {
    ChessGame chess = ChessGame();
    chess.load("newgame.txt");
    int test = chess.checkForStalemate();
    assert(test == 0);
}

void testCheckForStalemateWorks() {
    ChessGame chess = ChessGame();
    chess.load("currentStalemate.txt");
    int test = chess.checkForStalemate();
    assert(test == 1);
}

void testValidMoveFails() {
    ChessGame chess = ChessGame();
    chess.load("newgame.txt");
    //incorrectly move pawn
    Position start = Position(2, 1);
    Position end = Position(3, 3);
    Piece * pie = chess.getPiece(start);
    int test = pie->validMove(start, end, chess);
    assert(test == -1);
}

void testValidMoveWorks() {
    ChessGame chess = ChessGame();
    chess.load("newgame.txt");
    //correctly move knight
    Position start = Position(1, 0);
    Position end = Position(2, 2);
    Piece * pie = chess.getPiece(start);
    int test = pie->validMove(start, end, chess);
    assert(test == 1);
}
